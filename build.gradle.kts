plugins {
    id("org.jetbrains.kotlin.jvm").version("1.5.0")
    id("org.gradle.maven-publish")
}

group = "com.gitlab.ballysta"
version = "1.0.12"

repositories { mavenCentral() }

dependencies {
    testImplementation(kotlin("test"))
    testImplementation("io.mockk:mockk:1.12.0")
}

tasks.test { useJUnitPlatform() }

publishing {
    publications {
        create<MavenPublication>(project.name) {
            artifactId = project.name.toLowerCase()
            from(components["java"])
        }
    }
}