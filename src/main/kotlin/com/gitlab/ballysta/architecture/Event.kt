package com.gitlab.ballysta.architecture

import java.util.*
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

typealias Event<Listener> = Toggled.(Listener) -> (Unit)

open class TreeEvent<Listener> : (Toggled, Listener) -> (Unit) {
    var iterating = false
    val listeners = TreeMap<Int, Listener>()
    val changes = ArrayList<Pair<Int, Listener?>>()
    var index = 0

    inline fun forEach(block: (Listener) -> (Unit)) {
        iterating = true
        listeners.values.forEach(block)
        iterating = false
        changes.removeIf { (current, listener) ->
            if (listener == null) listeners -= current
            else listeners[current] = listener
            true
        }
    }

    override fun invoke(scope: Toggled, listener: Listener) {
        val current = index++
        scope.onEnabled(scope) {
            if (iterating) changes.add(current to listener)
            else listeners[current] = listener
        }
        scope.onDisabled(scope) {
            if (!iterating) listeners -= current
            else changes.add(current to null)
        }
    }
}

operator fun TreeEvent<() -> (Unit)>
        .invoke()
        = forEach { it() }
operator fun <First>
        TreeEvent<(First) -> (Unit)>
        .invoke(first: First)
        = forEach { it(first) }
operator fun <First, Second>
        TreeEvent<(First, Second) -> (Unit)>
        .invoke(first: First, second: Second)
        = forEach { it(first, second) }
operator fun <First, Second, Third>
        TreeEvent<(First, Second, Third) -> (Unit)>
        .invoke(first: First, second: Second, third: Third)
        = forEach { it(first, second, third) }
operator fun <First, Second, Third, Fourth>
        TreeEvent<(First, Second, Third, Fourth) -> (Unit)>
        .invoke(first: First, second: Second, third: Third, fourth: Fourth)
        = forEach { it(first, second, third, fourth) }
operator fun <First, Second, Third, Fourth, Fifth>
        TreeEvent<(First, Second, Third, Fourth, Fifth) -> (Unit)>
        .invoke(first: First, second: Second, third: Third, fourth: Fourth, fifth: Fifth)
        = forEach { it(first, second, third, fourth, fifth) }
operator fun <First, Second, Third, Fourth, Fifth, Sixth>
        TreeEvent<(First, Second, Third, Fourth, Fifth, Sixth) -> (Unit)>
        .invoke(first: First, second: Second, third: Third, fourth: Fourth, fifth: Fifth, sixth: Sixth)
        = forEach { it(first, second, third, fourth, fifth, sixth) }

private inline fun <Type> TreeEvent<Type>.cancelled(
    short: Boolean, crossinline block: (Type) -> (Boolean)
): Boolean {
    var cancelled = false
    forEach {
        cancelled = !block(it)
        if (cancelled && short)
            return true
    }
    return cancelled
}

operator fun TreeEvent<() -> (Boolean)>
    .invoke(short: Boolean = true)
    = cancelled(short) { it() }
operator fun <First>
    TreeEvent<(First) -> (Boolean)>
    .invoke(first: First, short: Boolean = true)
    = cancelled(short) { it(first) }
operator fun <First, Second>
    TreeEvent<(First, Second) -> (Boolean)>
    .invoke(first: First, second: Second, short: Boolean = true)
    = cancelled(short) { it(first, second) }
operator fun <First, Second, Third>
    TreeEvent<(First, Second, Third) -> (Boolean)>
    .invoke(first: First, second: Second, third: Third, short: Boolean = true)
    = cancelled(short) { it(first, second, third) }
operator fun <First, Second, Third, Fourth>
    TreeEvent<(First, Second, Third, Fourth) -> (Boolean)>
    .invoke(first: First, second: Second, third: Third, fourth: Fourth, short: Boolean = true)
    = cancelled(short) { it(first, second, third, fourth) }
operator fun <First, Second, Third, Fourth, Fifth>
    TreeEvent<(First, Second, Third, Fourth, Fifth) -> (Boolean)>
    .invoke(first: First, second: Second, third: Third, fourth: Fourth, fifth: Fifth, short: Boolean = true)
    = cancelled(short) { it(first, second, third, fourth, fifth) }
operator fun <First, Second, Third, Fourth, Fifth, Sixth>
    TreeEvent<(First, Second, Third, Fourth, Fifth, Sixth) -> (Boolean)>
    .invoke(first: First, second: Second, third: Third, fourth: Fourth, fifth: Fifth, sixth: Sixth, short: Boolean = true)
    = cancelled(short) { it(first, second, third, fourth, fifth, sixth) }