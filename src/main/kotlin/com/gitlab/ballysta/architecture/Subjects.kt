package com.gitlab.ballysta.architecture

interface Subject<Type> : (Toggled, (Type) -> (Unit)) -> (Unit), (Type) -> (Unit)
interface PublishSubject<Type> : Subject<Type>, PublishedObservable<Type>

//TODO deal with the problem of published last being used in event calls.
fun <Type> PublishSubject(initial: Type) = object : PublishSubject<Type> {
    val event = TreeEvent<(Type) -> (Unit)>()
    @Volatile override var last = initial
    override fun invoke(scope: Toggled, observer: (Type) -> (Unit)) {
        event(scope, observer)
        scope.onEnabled(scope) { observer(last) }
    }
    override fun invoke(value: Type) {
        val current = last; event(value)
        //TODO should this be builtin or nah?
        if (current == last) last = value
    }
}