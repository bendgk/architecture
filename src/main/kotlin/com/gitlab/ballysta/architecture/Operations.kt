@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.ballysta.architecture

//--Map--
fun <Key, Value, To> Mutable<Key, Value>.mapKeys(
    to: (Key) -> (To), from: (To) -> (Key)
) = object : Mutable<To, Value> {
    override fun get(key: To) = this@mapKeys[from(key)]
    override fun set(key: To, value: Value?) = this@mapKeys.set(from(key), value)
    override val onChanged: ChangeEvent<To, Value> = {
        this@mapKeys.onChanged { key, from, to ->
            it(to(key), from, to)
        }
    }
    override val size get() = this@mapKeys.size
    override val keys get() = this@mapKeys.keys.map(to)
    override val values get() = this@mapKeys.values
}
fun <Key, Value, To> Mutable<Key, Value>.mapValues(
    to: (Value) -> (To), from: (To) -> (Value)
) = object : Mutable<Key, To> {
    override fun get(key: Key) = this@mapValues[key]?.let { to(it) }
    override fun set(key: Key, value: To?) = set(key, value?.let { from(it) })?.let { to(it) }
    override val onChanged: ChangeEvent<Key, To> = {
        this@mapValues.onChanged { key, from, to ->
            it(key, from?.let { to(it) }, to?.let { to(it) })
        }
    }
    override val size get() = this@mapValues.size
    override val keys get() = this@mapValues.keys
    override val values get() = this@mapValues.values.map(to)
}

fun <Key, Value, To> Mutated<Key, Value>.mapKeys(
    to: (Key) -> (To), from: (To) -> (Key)
) = object : Mutated<To, Value> {
    override fun get(key: To) = this@mapKeys[from(key)]
    override val onChanged: ChangeEvent<To, Value> = {
        this@mapKeys.onChanged { key, from, to ->
            it(to(key), from, to)
        }
    }
    override val size get() = this@mapKeys.size
    override val keys get() = this@mapKeys.keys.map(to)
    override val values get() = this@mapKeys.values
}
fun <Key, Value, To> Mutated<Key, Value>.mapValues(
    to: (Value) -> (To)
) = object : Mutated<Key, To> {
    override fun get(key: Key) = this@mapValues[key]?.let { to(it) }
    override val onChanged: ChangeEvent<Key, To> = {
        this@mapValues.onChanged { key, from, to ->
            it(key, from?.let { to(it) }, to?.let { to(it) })
        }
    }
    override val size get() = this@mapValues.size
    override val values get() = this@mapValues.values.map(to)
    override val keys get() = this@mapValues.keys
}

//--Filter--
fun <Key, Value> Mutated<Key, Value>.filter(
    condition: (Key, Value) -> (Boolean)
) = object : Mutated<Key, Value> {
    override fun get(key: Key) = this@filter[key]?.takeIf { condition(key, it) }
    override val onChanged: ChangeEvent<Key, Value> = { listener ->
        this@filter.onChanged { key, from, to -> when {
            from == null -> { if (condition(key, to!!)) listener(key, from, to) }
            to == null -> { if (condition(key, from)) listener(key, from, to) }
            condition(key, from) && !condition(key, to) -> listener(key, from, null)
            !condition(key, from) && condition(key, to) -> listener(key, null, to)
            condition(key, from) && condition(key, to) -> listener(key, from, to)
        } }
    }
    override val keys get(): Enumerator<Key> {
        val keys = this@filter.keys
        val values = this@filter.values
        return {
            var key = keys()
            var value = values()
            while (key != null && value != null && !condition(key, value)) {
                key = keys()
                value = values()
            }
            key
        }
    }
    override val values get(): Enumerator<Value> {
        val keys = this@filter.keys
        val values = this@filter.values
        return {
            var key = keys()
            var value = values()
            while (key != null && value != null && !condition(key!!, value!!)) {
                key = keys()
                value = values()
            }
            value
        }
    }
}
operator fun <Key, Value> Table<Key, Value>.minus(key: Key) = filter { it, _ -> it != key }

//--Laws--
fun <Key, Value> Mutated<Key, Value>.difference(
    other: Mutated<Key, *>
) = object : Mutated<Key, Value> {
    @JvmField val parent = this@difference
    override fun get(key: Key): Value? = if (key in other) null else parent[key]
    override val onChanged: Event<(Key, Value?, Value?) -> Unit> = { listener ->
        parent.onChanged { key, from, to ->
            if (key !in other) listener(key, from, to)
        }
        other.onChanged { key, from, to ->
            parent[key]?.let {
                if (from == null) listener(key, it, null)
                else if (to == null) listener(key, null, it)
            }
        }
    }
    override val keys get() = parent.keys.filter { other[it] == null }
    override val values get(): Enumerator<Value> {
        val keys = parent.keys; val values = parent.values
        return { values()?.takeIf { other[keys()!!] == null } }
    }
}
fun <Key, First, Second, To> Mutated<Key, First>.union(
    other: Mutated<Key, Second>,
    merge: (Key, First?, Second?) -> (To)
) = object : Mutated<Key, To> {
    @JvmField val parent = this@union
    override fun get(key: Key): To? {
        val one = parent[key]
        val two = other[key]
        return if(one == null && two == null)
            null else merge(key, one, two)
    }
    override val onChanged: ChangeEvent<Key, To> = { listener ->
        parent.onChanged { key, from, to ->
            val value = other[key]
            val a = if (value == null && from == null)
                    null else merge(key, from, value)
            val b = if (value == null && to == null)
                    null else merge(key, to, value)
            if (a != b) listener(key, a, b)
        }
        other.onChanged { key, from, to ->
            if (parent[key] == null) {
                val a = from?.let { merge(key, null, it) }
                val b = to?.let { merge(key, null, it) }
                if (a != b) listener(key, a, b)
            }
        }
    }
    override val keys get(): Enumerator<Key> {
        val one = parent.keys; val two = other.keys
        return { one() ?: two()?.takeIf { parent[it] == null } }
    }
    //FIXME likely incorrect.
    override val values get(): Enumerator<To> {
        val firstKeys = parent.keys; val secondKeys = other.keys
        val firstValues = parent.values; val secondValues = other.values
        return {
            firstKeys()?.let {
                merge(it, firstValues(), other[it])
            } ?: secondKeys()?.let {
                merge(it, null, secondValues())
            }
        }
    }
}
fun <Key, First, Second, To> Mutated<Key, First>.intersection(
    other: Mutated<Key, Second>,
    merge: (Key, First, Second) -> (To)
) = object : Mutated<Key, To> {
    @JvmField val parent = this@intersection
    override fun get(key: Key): To? {
        val one = parent[key]; val two = other[key]
        return if(one == null || two == null)
            null else merge(key, one, two)
    }
    override val onChanged: ChangeEvent<Key, To> = { listener ->
        parent.onChanged { key, from, to ->
            val value = other[key]
            val a = if (value == null || from == null)
                null else merge(key, from, value)
            val b = if (value == null || to == null)
                null else merge(key, to, value)
            if (a != b) listener(key, a, b)
        }
        other.onChanged { key, from, to ->
            val value = parent[key]
            val a = if (value == null || from == null)
                null else merge(key, value, from)
            val b = if (value == null || to == null)
                null else merge(key, value, to)
            if (a != b) listener(key, a, b)
        }
    }
    override val keys get() = parent.keys.filter { other[it] != null }
    override val values get() = object : Enumerator<To> {
        val keys = parent.keys; val values = parent.values
        override fun invoke(): To? {
            val key = keys() ?: return null
            val value = values() ?: return null
            return other[key]?.let { two ->
                merge(key, value, two)
            } ?: invoke()
        }
    }
}

//TODO replace this with a dedicated implementation
fun <Key, First, Second, To> Mutated<Key, First>.exclusive(
    other: Mutated<Key, Second>,
    merge: (Key, First?, Second?) -> (To)
) = union(other, merge) - intersection(other, merge)

//--Operators--
inline operator fun <Key, Value> Table<Key, Value>.minus(
    other: Mutated<Key, *>
) = difference(other)
inline operator fun <Key, Value> Mutated<Key, Value>.plus(
    other: Mutated<Key, Value>
) = union(other) { _, a, b -> a ?: b!! }

infix fun <Key, Value> Mutated<Key, Value>.or(
    other: Mutated<Key, *>
) = union(other) { _, a, b -> a ?: b!! }
infix fun <Key, Value> Mutated<Key, Value>.and(
    other: Mutated<Key, *>
) = intersection(other) { _, a, _ -> a }
infix fun <Key, Value> Mutated<Key, Value>.xor(
    other: Mutated<Key, Value>
) = exclusive(other) { _, a, b -> a ?: b!! }