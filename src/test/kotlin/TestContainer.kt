import com.gitlab.ballysta.architecture.*
import io.mockk.*
import org.junit.jupiter.api.Nested
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class TestContainer : Toggled by Component({ enable() }) {
    @Test fun `mutable should contain A to 1, B to 2 and C to 3`() {
        val table = MutableTable<String, Int>()
        table["A"] = 1
        table["B"] = 2
        table["C"] = 3

        assertEquals(table.size, 3)

        assertEquals(table["A"], 1)
        assertEquals(table["B"], 2)
        assertEquals(table["C"], 3)

        assertContains(table.keys, "A", "B", "C")
        assertContains(table.values, 1, 2, 3)
    }
    @Test fun `mutable should fire changes A to 1, B to 2 and C to 3`() {
        val values = MutableTable<String, Int>()

        val listener = mockk<(String, Int?, Int?) -> (Unit)>()
        every { listener(any(), any(), any()) } just Runs

        transient { values.onChanged(listener) }.enable()

        values["A"] = 1
        values["B"] = 2
        values["C"] = 3

        verify(exactly = 1) {
            listener("A", null, 1)
            listener("B", null, 2)
            listener("C", null, 3)
        }

        confirmVerified(listener)
    }

    @Nested inner class Operations {
        @Test fun `on each should be called and toggled for each entry`() {
            val values = MutableTable<String, Int>()

            val onEachListener = mockk<Toggled.(String, Int) -> (Unit)>()
            every { onEachListener(any(), any(), any()) } just Runs

            val onEnabledListener = mockk<(String, Int) -> (Unit)>()
            val onDisabledListener = mockk<(String, Int) -> (Unit)>()
            every { onEnabledListener(any(), any()) } just Runs
            every { onDisabledListener(any(), any()) } just Runs

            values["A"] = 1

            transient {
                values.onEach(onEachListener)
                values.onEach { key, value ->
                    onEnabled { onEnabledListener(key, value) }
                    onDisabled { onDisabledListener(key, value) }
                }
            }.enable()


            verify(exactly = 1) {
                onEachListener(any(), "A", 1)
                onEnabledListener("A", 1)
            }

            values["B"] = 2
            values["C"] = 3

            verify(exactly = 1) {
                onEachListener(any(), "B", 2)
                onEachListener(any(), "C", 3)
                onEnabledListener("B", 2)
                onEnabledListener("C", 3)
            }

            values["A"] = null
            values["C"] = null

            verify(exactly = 1) {
                onDisabledListener("A", 1)
                onDisabledListener("C", 3)
            }

            confirmVerified(onEachListener, onEnabledListener, onDisabledListener)
        }

        @Test fun `intersection should contain and notify about only entries with shared keys`() {
            val first = MutableTable<String, Int>()
            val second = MutableTable<String, Int>()

            val result = first.intersection(second) { _, a, b -> a + b }

            val listener = mockk<(String, Int?, Int?) -> (Unit)>()
            every { listener(any(), any(), any()) } just Runs
            transient { result.onChanged(listener)
                result.onChanged { key, from, to -> println("$key: $from - $to") } }.enable()

            first["A"] = 1
            first["B"] = 2

            second["A"] = 3
            second["C"] = 4

            assertEquals(result.size, 1)

            assertEquals(result["A"], 4)
            assertNull(result["B"])
            assertNull(result["C"])

            assertContains(result.keys, "A")
            assertContains(result.values, 4)

            verify(exactly = 1) { listener("A", null, 4) }

            first["A"] = 2
            second["A"] = null
            second["A"] = 4
            first["A"] = null
            second["A"] = null
            first["A"] = 1
            second["A"] = 4

            verify(exactly = 1) {
                listener("A", 4, 5)
                listener("A", 5, null)
                listener("A", null, 6)
                listener("A", 6, null)
                listener("A", null, 5)
            }

            first["D"] = 5
            first["E"] = 6
            second["E"] = 7

            verify(exactly = 1) { listener("E", null, 13) }

            assertContains(result.keys, "A", "E")
            assertContains(result.values, 5, 13)

            confirmVerified(listener)
        }
    }

    private fun <Type> assertContains(enumerator: Enumerator<Type>, vararg values: Type) {
        var i = 0; enumerator.forEach { value ->
            i++; assert(values.contains(value))
        }
        assertEquals(values.size, i)
        assertNull(enumerator())
    }
}
